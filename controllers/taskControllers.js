//Task is a module located in Task.js file. to access this:
const Task = require('../models/Task')

module.exports.createTaskController = (req,res)=>{

	console.log(req.body);

	//Created a new task object out of our Task model.
		//Check if there is a document with duplicate name field.
		//Model.findOne() is a Mongoose method similar to findOne in MongoDB
		//however, with Model.findOne(), we can process the result via our API.
		//.then() is able to capture the result of our query
			//,then() is able to process the result and when that result is returned, we can actually add another then() to process the next result
		//.catch() is able to capture the error of our query
		Task.findOne({name: req.body.name})
			.then(result => {
				console.log(result);
				if(result !== null && result.name === req.body.name){

					return res.send("Duplicate Task Found")

				} else{

					//newTask has added methods for use in our application.
					//use this task model a constructor. Note: without the () it will not work.
					let newTask = new Task({

						name: req.body.name

					})

						/*
						code replaced below:	
							newTask.save((saveErr, savedTask)=>{

								console.log(savedTask);

								if(saveErr){
									return console.error(saveErr);
								}else{
									return res.send("New Task Created")
								} 

							})
						*/
					newTask.save()
					.then(result => res.send(result))
					.catch(err => res.send(err))

				}
			})

			.catch(err => res.send(err));
}			

module.exports.getAllTasksController = (req,res)=>{
	//Task (your model) is your connection to your db collection (tasks)
	//Model.find() is a mongoose method similar to mongodb's find(). It is able to retrieve all documents that will match the criteria.	
	Task.find({})
		/*(err,result)=>{
			console.log(err);
			console.log(result);

			if(err !== null){
				return console.log(err);
			} else{
				return res.send(result);
			}
		})*/
		.then(result => res.send(result))
		.catch(err => res.send(err))

}

module.exports.getSingleTaskController = (req,res) => {

	console.log(req.params.id)//the id passed from your url.
	//mongoose queries such as Model.find(), Model.findOne(), Model.findById() has a second argument for projection. It works the same as in MongoDB.
	Task.findById(req.params.id,{_id:0,name:1}) //can also add projection fields
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

module.exports.completeSingleTaskController = (req,res) => {

	console.log(req.params.id)

	let updates = {

		status: "Complete"

	}

	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
		.then(updatedTask => res.send(updatedTask))
		.catch(err => res.send(err))

}

module.exports.cancelSingleTaskController = (req,res) => {

	console.log(req.params.id)

	let updates = {

		status: "Cancelled"

	}

	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
		.then(updatedTask => res.send(updatedTask))
		.catch(err => res.send(err))

}

//err = null - no error
			//result = has value - the name already exists in the database.
			//result = null, no result found.

	

	//.save() is a method from an object created by a model.
		//can be used as long as the object is made by the constructor.

	//This will then allow us to save our document into our collection.
	//.save() can have an anonymous function and this can take 2 paramenters.
	// saveErr receives an error object if there was an error in creating our document.
	//savedTask - is our saved document.
	//__v - version
	//_id - Object Id - mongoDB auto assigned ID. 
	//Comment out:
	/**/

