const User = require('../models/User');

module.exports.createUserController = (req,res)=>{

	console.log(req.body);

	User.findOne({username: req.body.username},(err,result) => {

			console.log(err);
			console.log(result);
			
			if(result !== null && result.username === req.body.username){

				return res.send("Duplicate username found!")

			} else{

				let newUser = new User({

					username: req.body.username,
					password: req.body.password

				})

				newUser.save((saveErr, savedUser)=>{

					console.log(savedUser);

					if(saveErr){
						return console.error(saveErr);
					}else{
						return res.send("Successful Registration!")
					} 

				})

			}	

	})

}

module.exports.getAllUsersController = (req,res) => {

	User.find({})
	.then(result=>res.send(result))
	.catch(err=>res.send(err))

}

module.exports.getSingleUserController = (req,res) => {

	//mongoose has a query called findById() which works like find({_id: "id"})
	console.log(req.params.id)//the id passed from your url.
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateSingleUserController = (req,res) => {

	console.log(req.params.id)
	/*
		Model.findByIdAndUpdate will do 2 things - it finds a single item using its Id(string) and add the update

		Model.findByIdAndUpdate(id,{updates},{new:true})
	*/

	//updates object will contain the field and the value we want to update
	let updates = {

		username: req.body.username

	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	//note: by not putting in the {new:true} - it will still update, but postman will still display the old value(in the clients side. but works at the backend)
	//By default, without the 3rd argument, findByIdAndUpdate returns the decument bef the updates were added.
	//new:true - allows us to return the updated document.

	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

}