const express = require("express");
const mongoose = require("mongoose");


const app = express();

const port = 4000;

//paste mongodb connection - must put
mongoose.connect("mongodb+srv://kimlu10239:Victory11@cluster0.qvgky.mongodb.net/todoList123?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true	
	}

);
//notifs if mongodb connection is success or fail:
let db = mongoose.connection;
//console.error.bind(console,<message>) - print error in both terminal and browser.
db.on("error",console.error.bind(console,"connection error."));
//output message in terminal if connection is successful)
db.once("open", ()=>console.log("connected to mongoDB"));


//Middleware - middleware in expressjs context, are methods, functions that acts and add features to your application.
//handlejson data from our client.
app.use(express.json());

const taskRoutes = require('./routes/taskRoutes');
console.log(taskRoutes);

//a middleware to group all of our routes starting their endpoints with /tasks - so when accessing routes - it is middleware plus routes.(router.post + app.use) so leave / in router.post instead.
app.use('/tasks',taskRoutes);

const userRoutes = require('./routes/userRoutes');
/*console.log(userRoutes);*/
app.use('/users',userRoutes);		


app.get('/hello',(req,res) => {

	res.send("Hello from our new Express API!");

})

app.listen(port, () => console.log(`Server is running at port ${port}`));