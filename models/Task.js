//when putting in separate file: needs to require mongoose too.
const mongoose = require('mongoose');


//Mongoose Schema
	/*
		Schemas, in mongoose, determin the structure of the documents. It acts as a blueprint to how data/entries in our collection should look like. Gone are the days when you worry if the entry has 'stock' or 'stocks' as field names. Mongoose itself will notify us and disallow us to create documents that do not match the schema.
	*/
	const taskSchema = new mongoose.Schema({
		//defines the fields for the task document
		// value of name to be a string.
		// status has a curly brace - to accomodate default: - value should be a string.
			//default: <default value> - self explanatory. 
		name: String,
		status: {
			type: String,
			default: "pending"
		}

	});

	//Mongoose Model
	/*
		Models are used to connect your API to the corresponding collection in your database. It is a representation of the Task documents to be created into a new tasks collection.

		Models uses schemas to create/instantiate objects that correspond to the schema. By default, when creating the collection from your model, the collection name will be pluralized (ENGLISH Only)

		mongoose.model(<nameofCollectionInAtlas>,taskSchema)

	*/

	/*const Task = mongoose.model("Task", taskSchema);*/

	//How to export model - to be used in other files
	//module.exports will allow us to export file into another js file within our application.
	module.exports = mongoose.model("Task", taskSchema);