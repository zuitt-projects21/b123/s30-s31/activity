//app does not exist here. so require express.
const express = require("express");
//Router() from express, allows us to access to HTTP method routes
//this acts as a middleware and our routing system.
const router = express.Router();
//Routes should only be concerned with our endpoints and our methods.
//The action to be done once a route is accessed should be in a separate file, it should be in our controllers (so functions, etc)

const taskControllers = require('../controllers/taskControllers');
//require this file to be able to use createTaskController and getAllTasksController^
//modules are objects. so you need to deconstruct first to get createTaskController and getAllTasksControllers
const {
	createTaskController,
	getAllTasksController,
	getSingleTaskController,
	completeSingleTaskController,
	cancelSingleTaskController
} = taskControllers;
console.log(taskControllers);

router.post('/',createTaskController)
			
router.get('/', getAllTasksController)

router.get('/:id', getSingleTaskController)

router.put('/complete/:id', completeSingleTaskController)
router.put('/cancel/:id', cancelSingleTaskController)


//router holds all of our routes and can be exported and imported into another file.
module.exports = router;