//import express and Router()
const express = require('express');
const router = express.Router();

//import the user controllers
	//set path
const userControllers = require('../controllers/userControllers');
	//put all modules in a single controller
const {createUserController,getAllUsersController,getSingleUserController,updateSingleUserController} = userControllers;

router.post('/',createUserController);
router.get('/',getAllUsersController);
//getSingleUser
router.get('/:id',getSingleUserController);
router.put('/:id',updateSingleUserController);



//export router
module.exports = router;	